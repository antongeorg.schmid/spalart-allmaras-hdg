#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ngsolve import *
from netgen.geom2d import SplineGeometry


class NavierStokesHDG():
    '''
    Navier Stokes Solver using a HDG discretization for the stokes term and an upwind DG formulation for the convection.

    Parameters
    ----------

    mesh: ngsolve.comp.Mesh
        computational mesh
    order: int
        order of the element space
    quads: bool
        flag for a quad mesh
    nu_0: float
        kinetic viscosity of the fluid
    uin: ngsolve.fem.CoefficientFunction
        inflow velocity set on inlet (default is CoefficientFunction((0,0)))
    uinitial: ngsolve.fem.CoefficientFunction
        initial velocity field. This is overwritten by ComputeStationary (default is CoefficientFunction((0,0)))
    inlet: str
        regular exoression containing the inlet boundaries (default is "")
    dirichlet: str
        regular expression containing the Dirichlet boundaries (default is "")
    neumann: str
        regular expression containing neumann boundaries (default is "")
    tau_w: ngsolve.fem.CoefficientFunction
        CoefficientFunction for sheerstresses. For tau_w = 0 this enforces zero neumann b.c.. For tau_w != 0 this is not tested. (default is "")
    ComputeStationary: bool
        flag for computing a stationary solution as initial values, not in use for periodic geometries (default is True)

    '''
    def __init__(self, mesh, order, quads, nu_0, uin = CoefficientFunction((0,0)), uinitial = CoefficientFunction((0,0)), inlet = "", dirichlet = "", neumann = "", tau_w = 0, ComputeStationary = True):
        
        # HDG spaces for Stokes equation
        self.mesh = mesh; self.order = order; self.quads = quads; self.inlet = inlet; self.dirichlet = dirichlet; self.neumann = neumann; self.tau_w = tau_w
        V1 = HDiv ( mesh, order = order, dirichlet = inlet + "|" + dirichlet +"|" + neumann )
        V2 = FESpace ( "vectorfacet", mesh, order = order,
              dirichlet = inlet + "|" + dirichlet +"|" + neumann )
        if(self.quads == False):
            self.Q = L2( mesh, order = order-1)
        else:
            self.Q = L2( mesh, order = order)
        self.V = FESpace ([V1,V2,self.Q])

        self.u, self.uhat, self.p = self.V.TrialFunction()
        self.v, self.vhat, self.q = self.V.TestFunction()

        self.nu_0 = nu_0 # background viscosity
        self.nu = nu_0 # effective viscosity
        self.n = specialcf.normal(mesh.dim)
        self.h = specialcf.mesh_size

        # Mass matrix

        self.m = BilinearForm(self.V , symmetric=False)
        self.m += SymbolicBFI( self.u * self.v )
        self.m.Assemble()

        # DG Space for convection

        self.gfu = GridFunction(self.V)

        self.wallsheerstressform = LinearForm(self.V)
        # Initital conditions for velocity-pressure pair
        if(self.inlet != ""):
            self.uin = uin
            self.gfu.components[0].Set(self.uin, definedon=mesh.Boundaries(self.inlet))
            if(ComputeStationary == True):
                self._SetandAssembleStokes()
                self.wallsheerstressform.Assemble()
                invstokes = self.stokes.mat.Inverse(self.V.FreeDofs(), inverse="pardiso")

                res = self.gfu.vec.CreateVector()
                res.data = self.gfu.vec
                self.gfu.vec.data += invstokes @ -self.stokes.mat * res
            else:
                self.uinitial = uinitial
                self.gfu.components[0].Set(self.uinitial)
        else:
            self.uinitial = uinitial
            self.gfu.components[0].Set(self.uinitial)

        # n = self.n
        # def tang(vec):
        #     return vec - (vec*n)*n
        # self.wallsheerstressform += self.nu * InnerProduct ( tang(tau_0), tang(self.vhat-self.v)*n ) * ds (skeleton = True, definedon = self.mesh.Boundaries("wall|cyl"))

        Draw( self.gfu.components[0], mesh, "velocity" )
        Draw( self.gfu.components[2], mesh, "pressure" )
        Draw( Norm(self.gfu.components[0]), mesh, "absvel(hdiv)")

        self.prevtau = 0
        self.prevsubsteps = 0

        self.xstressgfu = GridFunction(self.V)
        self.xstressgfu.components[0].Set( (grad( self.gfu.components[0])[0], grad(self.gfu.components[0])[1] ))
        Draw(self.xstressgfu.components[0], mesh, "x-sheerstress")

        self.ystressgfu = GridFunction(self.V)
        self.ystressgfu.components[0].Set( (grad( self.gfu.components[0])[2], grad(self.gfu.components[0])[3] ))
        Draw(self.ystressgfu.components[0], mesh, "y-sheerstress")


    def _FlagNeumannBoundaries(self):

        fesff = FacetFESpace(self.mesh,order=0)
        interface_indicator = BitArray(fesff.ndof)
        interface_indicator[:] = False
        interface_indicator |= fesff.GetDofs(self.mesh.Boundaries(self.neumann))
        self.gf_ind = GridFunction(fesff)
        for i in range(fesff.ndof):
            if interface_indicator[i]:
                self.gf_ind.vec[i] = 0
            else:
                self.gf_ind.vec[i] = 1

    def _SetandAssembleStokes(self):

        """ Set and assemble the bilinear form for convection and stokes terms. """

        if(self.neumann != ""):
            self._FlagNeumannBoundaries()
        else:
            self.gf_ind = 1

        n = self.n
        def tang(vec):
            return vec - (vec*n)*n
        alpha = 4  # SIP parameter
        dS = dx(element_boundary=True)
        self.stokes = BilinearForm ( self.V, symmetric=False)
        self.stokes+= self.nu * InnerProduct ( Grad(self.u), Grad(self.v)) * dx
        self.stokes+= self.gf_ind * self.nu * InnerProduct ( Grad(self.u)*n,  tang(self.vhat-self.v) ) * dS
        self.stokes+= self.gf_ind * self.nu * InnerProduct ( Grad(self.v)*n,  tang(self.uhat-self.u) ) * dS
        self.stokes+= self.gf_ind * self.nu * alpha*self.order*self.order/self.h * InnerProduct(tang(self.vhat-self.v),  tang(self.uhat-self.u))*dS
        self.stokes+= (-div(self.u)*self.q -div(self.v)*self.p) * dx

        self.stokes+= (1-self.gf_ind) * self.nu * self.tau_w * InnerProduct( tang(self.u),tang(self.v)) * dS

        w = CoefficientFunction(self.gfu.components[0])
        advection = InnerProduct(Grad(self.u) * w, self.v)*dx + w*n * InnerProduct(tang(self.uhat - self.u), IfPos(w*n,tang(self.vhat),tang(self.v))) * dS
        self.stokes += advection

        self.stokes.Assemble()

    def DoTimeStep(self, tau, pressure_gradient = CoefficientFunction((0,0))):

        """
        Do time step.

        Parameters
        ----------
        tau: float
            time difference
        pressure_gradient: ngsolve.fem.CoefficientFunction
            pressure gradient for the next time step

        """

        grad_p = LinearForm(self.V)
        grad_p += pressure_gradient *self.v * dx
        grad_p.Assemble()

        self._SetandAssembleStokes()
        self.prevtau = tau;
        self.mstar = self.m.mat.CreateMatrix()
        self.mstar.AsVector().data = self.m.mat.AsVector() + tau * self.stokes.mat.AsVector()
        try:
            self.inv = self.mstar.Inverse(self.V.FreeDofs(), inverse="pardiso")
        except:
            self.inv = self.mstar.Inverse(self.V.FreeDofs(), inverse="umfpack")

        r = self.gfu.vec.CreateVector()
        r.data = grad_p.vec -self.stokes.mat * self.gfu.vec
        self.gfu.vec.data += tau * self.inv *r

        #self.stressgfu.components[0].Set((grad(CoefficientFunction(self.gfu.components[0]))[0],grad(CoefficientFunction(self.gfu.components[0]))[1] ))

    def DoRANSiteration(self, tau, iterations, nu_t, pressure_gradient = CoefficientFunction((0,0))):

        """
        Do time step.

        Parameters
        ----------
        tau: float
            time difference
        nu_t: ngsolve.fem.CoefficientFunction
            eddy viscosity for the next timestep
        pressure_gradient: ngsolve.fem.CoefficientFunction
            pressure gradient for the next time step

        """

        with TaskManager():
            grad_p = LinearForm(self.V)
            grad_p += pressure_gradient * self.v* dx
            grad_p.Assemble()

            self.nu = self.nu_0 + nu_t
            self._SetandAssembleStokes()
            #self.SetConvection()
            self.mstar = self.m.mat.CreateMatrix()
            self.mstar.AsVector().data = self.m.mat.AsVector() + tau * self.stokes.mat.AsVector()
            try:
                self.inv = self.mstar.Inverse(self.V.FreeDofs(), inverse="pardiso")
            except:
                self.inv = self.mstar.Inverse(self.V.FreeDofs(), inverse="umfpack")
            for i in range(iterations):
                r = self.gfu.vec.CreateVector()
                r.data = grad_p.vec -self.stokes.mat * self.gfu.vec
                self.gfu.vec.data += tau * self.inv *r

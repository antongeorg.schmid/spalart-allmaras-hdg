#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.getcwd())
sys.path.append('../../')

from ngsolve import *
from netgen.geom2d import SplineGeometry
from DNS.NavierStokesHDG import NavierStokesHDG

geo = SplineGeometry()
geo.AddRectangle( (0, 0), (2, 0.41), bcs = ("wall", "outlet", "wall", "inlet"))
geo.AddCircle ( (0.2, 0.2), r=0.05, leftdomain=0, rightdomain=1, bc="cyl")
mesh = Mesh( geo.GenerateMesh(maxh=0.08))
mesh.Curve(3)
Draw(mesh)
uin = CoefficientFunction(( 1.5 *4 * (y * (0.41 -y))/0.41**2  ,0))

t = 0
tend = 100
tau = 0.1
substeps = 10
nu = 1e-3
order = 3

navierstokes = NavierStokesHDG(mesh = mesh, order = order, quads = False, nu_0 = nu, uin = uin, uinitial = 0, inlet = "inlet", dirichlet = "wall|cyl", neumann = "", tau_w = 0)

Draw( navierstokes.gfu.components[0], mesh, "velocity" )
Draw( Norm(navierstokes.gfu.components[0]), mesh, "absvel(hdiv)")

while(t < tend):
    #navierstokes.DoTimeStep(tau)
    #t += tau

    iterations = 100
    navierstokes.DoRANSiteration(tau = tau, iterations = 10, nu_t = 0)
    t += tau * iterations
    print ("\r  t =", t, end="")
    Redraw(blocking=True)

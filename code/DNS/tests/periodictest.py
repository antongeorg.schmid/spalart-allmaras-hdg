#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import numpy as np
sys.path.append(os.getcwd())
sys.path.append('../../')
from ngsolve import *
from ngsolve.meshes import *
from netgen.geom2d import SplineGeometry

from DNS.NavierStokesHDG import NavierStokesHDG

Ny = 10
h = 0.41
gamma = 2
mesh = MakeStructured2DMesh(nx=10,ny = 30,periodic_x=True, mapping = lambda x,y: (x,h/2*(1-np.tanh(gamma*(1-2*y))/np.tanh(gamma))))
uinitial = CoefficientFunction( (1.5*4*y*(h-y)/(h*h), 0) )

t = 0
tend = 10
tau = 0.1
nu = 0.001

order = 3
navierstokes = NavierStokesHDG(mesh = mesh, order = order,  nu_0 = nu, uin = None, uinitial = uinitial, quads = True, dirichlet = "bottom|top|wall", neumann = "", tau_w = 0)

while(t < tend):
    #navierstokes.DoTimeStep(tau, pressure_gradient = CoefficientFunction((0.07,0)))
    #t += tau

    iterations = 10
    navierstokes.DoRANSiteration(tau = tau, iterations = iterations, nu_t = 0, pressure_gradient = CoefficientFunction((0.07,0)))
    t += tau * iterations
    print ("\r  t =", t, end="")
    Redraw(blocking=True)

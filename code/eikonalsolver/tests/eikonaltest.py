#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
#sys.path.append(os.getcwd())
sys.path.append("../../")

from ngsolve import *
from netgen.geom2d import SplineGeometry
from eikonalsolver.eikonalsolver import eikonalsolver


# define test geometry
geo = SplineGeometry()
geo.AddRectangle( (0, 0), (2, 0.41), bcs = ("wall", "outlet", "wall", "inlet"))
geo.AddCircle ( (0.2, 0.2), r=0.05, leftdomain=0, rightdomain=1, bc="cyl", maxh=0.02)
mesh = Mesh( geo.GenerateMesh(maxh=0.07))
mesh.Curve(3)

distance = eikonalsolver(mesh = mesh, order = 3, diffusioncoefficient = 0.1, BND = "wall|cyl")

TOL = 1e-3
while(1):
    distance.DoIteration()
    if(distance.diff < TOL):
        break

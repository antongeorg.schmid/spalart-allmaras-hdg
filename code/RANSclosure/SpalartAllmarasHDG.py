#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ngsolve import *
from netgen.geom2d import SplineGeometry
import numpy as np

class SpalartAllmarasHDG:

    """
    Implementation of the Spallart-Allmaras negative model using a Hybrid Discontinuous Galerkin method.

    Computing a new timestep is done by calling the method DoTimeStep.

    Parameters
    ----------

    mesh: ngsolve.comp.Mesh
        computational mesh
    inlet: str
        inlet of the geometry, use empty string for periodic geometries
    dirichlet: str
        inlet and walls in the geometry
    walldistance: ngsolve.fem.CoefficientFunction
        given a point, this returns its distance to the closest wall
    order: int
        order of the element space
    nu_0: float
        kinetic viscosity of the fluid
    inflow: ngsolve.fem.CoefficientFunction
        Dirichlet condition for inflow of the Spallart-Allmaras working variable
    initialfield: ngsolve.fem.CoefficientFunction
        initial field configuration of the Spallart-Allmaras working variable
    epsilon: float
        regularization parameter for the destruction term
    PicardTOL: float
        tolerence for stopping the Picard iteration

    Remarks
    -------

    In the current version, trip terms are not implemented.
    """

    def __init__(self, mesh, inlet, dirichlet, walldistance, order,  nu_0, inflow , initialfield, epsilon, PicardTOL):

        ###Definition of constants
        self.nu_0 = nu_0
        self.c_b1 = 0.1355
        self.c_b2 = 0.622
        self.sigma = 2./3
        self.kappa = 0.41
        self.c_w2 = 0.3
        self.c_w3 = 2
        self.c_v1 = 7.1
        self.c_t1 = 1
        self.c_t2 = 2
        self.c_t3 = 1.2
        self.c_t4 = 0.5
        self.c_w1 = self.c_b1/self.kappa**2 + (1+self.c_b2)/self.sigma
        ###declaration of Coefficient Functions
        self.f_v1 = CoefficientFunction(0)
        self.Chi = CoefficientFunction(0)
        self.S = CoefficientFunction(0)
        self.f_v2 = CoefficientFunction(0)
        self.f_w = CoefficientFunction(1)
        self.g = CoefficientFunction(0)
        self.r = CoefficientFunction(0)
        self.f_t2 = CoefficientFunction(0)
        self.vort = CoefficientFunction(0)
        self.nu = CoefficientFunction(0)
        self.f_t1 = CoefficientFunction(0)
        self.g_t = CoefficientFunction(0)
        self.d_t = CoefficientFunction(0)

        ###HDG Setup
        self.mesh = mesh
        self.d = walldistance
        Draw(self.d, self.mesh, "walldistance")
        self.order = order
        self.dt = 0
        self.wind = CoefficientFunction((0,0))
        self.inlet = inlet
        self.inflow = inflow
        self.eps = epsilon
        self.PicardTOL = PicardTOL

        V = Periodic(L2(mesh, order = self.order)) ## The periodic operator has no effect on non-periodic geometry
        F = Periodic(FacetFESpace(mesh, order=self.order, dirichlet=inlet + "|" +dirichlet))
        self.RES = Periodic(H1(mesh, order = self.order, dirichlet =inlet + "|" +dirichlet ))
        self.nu_interp = GridFunction(self.RES)
        self.vortspace = Periodic(VectorH1(mesh, order = self.order, dirichlet = dirichlet))
        self.fes = FESpace([V,F])
        self.gfu = GridFunction(self.fes)
        self.gfu_old = GridFunction(self.fes)
        self.residualgfu = GridFunction(self.fes)
        self.H1residualgfu = GridFunction(self.RES)
        self.vortgfu = GridFunction(self.vortspace)

        self.h = specialcf.mesh_size
        self.n = specialcf.normal(self.mesh.dim)
        self.alpha = 20
        self.u, self.uhat = self.fes.TrialFunction()
        self.v, self.vhat = self.fes.TestFunction()
        self.jump_u = self.u-self.uhat
        self.jump_v = self.v-self.vhat

        ###Definition of Forms for Linear Advection Diffusion
        self.implicitform = BilinearForm(self.fes)
        self.artificial_diffusion = 0
        self.explicitform = BilinearForm(self.fes)

        ###Declaration of Form for Forcing

        self.f = LinearForm(self.fes)

        ###setting initial conditions and inflow condition

        if(self.inlet != ""):
            self.gfu.components[1].Set(self.inflow, definedon=mesh.Boundaries(self.inlet))

        self.gfu.components[0].Set(initialfield)

        ###Setup for visualization

        Draw(self.gfu.components[0],self.mesh, "working_viscosity")
        Draw(self.residualgfu.components[0],self.mesh, "residual")
        Draw(self.H1residualgfu, self.mesh, "H1residual")
        self.gridvorticity = GridFunction(self.fes)
        Draw(self.gridvorticity.components[0], self.mesh, "vorticity")

        self.eddygfu = GridFunction(self.fes)
        Draw(self.eddygfu.components[0], self.mesh, "eddy_viscosity")

        self.eddyviscosity = CoefficientFunction(0)

        self.artificialdiffusiongfu = GridFunction(self.fes)
        Draw(self.artificialdiffusiongfu.components[0], self.mesh, "Artificial_Diffusion")

        self.Dgfu = GridFunction(self.fes)
        Draw(self.Dgfu.components[0], self.mesh, "Destruction")

        self._ComputeInnerProduct()

    def _ComputeCoefficientFunctions(self):

        """
        Computes new Coefficient Functions for the Production and Destruction terms
        """
        self.nu = self.gfu.components[0]
        self.gradnu = grad(self.nu)
        self.Chi = self.nu/self.nu_0
        self.f_v1 = self.Chi**3 /(self.Chi**3 +self.c_v1**3)
        self.S = (self.vort**2)**0.5
        self.f_v2 = 1 - self.Chi/(1+self.Chi*self.f_v1)
        self.nu_over_d = self.nu/(self.d + self.eps)
        self.nu_over_d2 = self.nu_over_d/(self.d +self.eps)

        #self.Shat = IfPos(self.S + self.nu_over_d2 /self.kappa**2*self.f_v2, self.S + self.nu_over_d2 /self.kappa**2*self.f_v2, 0) ###only allow positive Shat
        self.Sbar = self.nu /(self.d+self.eps)**2 /self.kappa**2*self.f_v2
        c_v2 = 0.7
        c_v3 = 0.9
        S = self.S
        Sbar = self.Sbar
        self.Shat = IfPos(c_v2*S + Sbar, S + Sbar, S + S *(c_v2**2 * S + c_v3 * Sbar)/((c_v3 - 2*c_v2)*S - Sbar))
        self.r = IfPos(self.nu_over_d2/(self.Shat) / self.kappa**2 -10, 10, self.nu_over_d2/(self.Shat) / self.kappa**2)
        #self.r = self.nu/self.d**2 /self.Shat /self.kappa**2
        self.g = self.r + self.c_w2 * (self.r**6-self.r)
        #self.g = self.r
        #self.f_w = self.g * ((1+self.c_w3**6)/(self.g**6 + self.c_w3**6))**(1./6.)
        self.f_w =1
        #self.f_w = (1+ self.c_w3**6)**(0.16666666)* (self.g**6/(self.g**6 + self.c_w3**6))**.16666
        #self.f_w = (self.g**6/(self.g**6 + self.c_w3**6))**.16666
        #self.f_w = (1+ self.c_w3**6)**(0.16666666)*(1- (self.c_w3**6/((self.g**6 + self.c_w3**6))))
        #self.f_t2 = self.c_t3 * exp(-self.c_t4*self.Chi**2)
        self.f_t2 = 0
    def _SetImplicitTerms(self):

        """
        Assembles a Bilinear Form for the nonlinear diffusion.
        """
        self.implicitform = BilinearForm(self.fes)
        dS = dx(element_boundary=True)
        self.nu_interp.Set(self.gfu.components[0])
        #self.nu = self.gfu.components[0]
        self.nu = self.nu_interp
        diffusion_coefficient = self.nu + self.nu_0 + self.artificial_diffusion

        diffusion = diffusion_coefficient* grad(self.u)*grad(self.v)*dx + \
                        diffusion_coefficient*(-grad(self.u)*self.n*self.jump_v - grad(self.v)*self.n*self.jump_u)*dS \
                        + diffusion_coefficient*self.alpha*(self.order+1)**2/self.h*self.jump_u*self.jump_v*dS

        n = self.n
        beta = self.sigma * self.wind - self.c_b2 * grad(self.nu)
        uup = IfPos(beta *n, self.u, self.uhat)
        advection = beta * grad(self.u) * self.v*dx + beta*n *(uup - self.u) * self.v * dS + IfPos(beta*n, self.uhat - self.u, 0)*self.vhat*dS
        #gradsquared = self.c_b2* grad(self.nu)*grad(self.u)* self.v*dx

        self.implicitform += 1./self.sigma * ( advection + diffusion)
        self.implicitform += self.c_w1* self.f_w*self.nu/(self.d+self.eps) * self.u /(self.d + self.eps) *self.v * dx
        #self.implicitform += -self.c_b2 * grad(self.nu) * grad(self.u) * self.v * dx
        with TaskManager():
            self.implicitform.Assemble()


    def _ImplicitEuler(self):

        """
        Computes the matrices for the implicetely treated parts of the equation.

        Returns
        -------

        mstar: ngsolve.la.SparseMatrixd
        invmstar: ngsolve.la.BaseMatrix
            Inverse of mstar
        """
        m = BilinearForm(self.fes)
        m += self.u*self.v *dx
        m.Assemble()

        mstar = m.mat.CreateMatrix()
        mstar.AsVector().data = m.mat.AsVector() + self.dt *self.implicitform.mat.AsVector()
        with TaskManager():
            try:
                invmstar = mstar.Inverse(self.fes.FreeDofs(), "pardiso")
            except:
                print("pardiso unavailable")
                invmstar = mstar.Inverse(self.fes.FreeDofs(), "umfpack")
        return mstar, invmstar

    def _SetExplicitTerms(self):

        """Sets a Bilinear Form for the explicitly treated parts of the equation, not in use"""

        # w = self.wind
        # dS = dx(element_boundary=True)
        # self.explicitform = BilinearForm(self.fes)

        ####DG version

        # uup = IfPos(w*self.n, self.u, self.u.Other(self.inflow))
        # self.explicitform += -w * self.u * grad(self.v)*dx + w*self.n*uup*self.v *dS
        #
        # b = self.gradnu
        # difup = IfPos(b*self.n, self.u, self.u.Other(0))
        # difterm = b * grad(self.u) * self.v* dx + b *self.n * (difup-self.u) *self.v * dS
        #
        # self.explicitform += difterm
    def _SetandAssembleForces(self):

        """
        Assembles a linear form for the production and destruction terms.
        """

        self.f = LinearForm(self.fes)
        #self.vortgfu.Set(self.c_b1*(1-self.f_t2)*self.Shat*self.nu)
        self.P = self.c_b1*(1-self.f_t2)*self.Shat*self.nu

        self.D_w = IfPos(self.d, self.c_w1* self.f_w*self.nu_over_d**2, 0)
        self.D_t = -self.c_b1/self.kappa**2 * self.f_t2* self.nu_over_d**2
        #nu = self.gfu.components[0]
        #gradterm = self.c_b2 * grad(nu) * grad(nu)
        self.f += self.P*self.v*dx - self.D_t*self.v*dx# + gradterm *self.v  *dx# - self.D_w *dx  #note that the other terms are treated implicitly
        self.f.Assemble()



    def _ComputeEddyViscosity(self):

        """Computes the Eddyviscosity used for the coupling with a Navier Stokes Solver."""

        self._ComputeCoefficientFunctions()
        self.eddyviscosity = CoefficientFunction(self.gfu.components[0])* self.Chi**3 / (self.Chi**3 + self.c_v1**3)


    def _SetandComputeLinearSystem(self):

        """Computes the linear system for the increment and returns it."""

        #self._SetExplicitTerms()   #not in use
        self._SetImplicitTerms()
        self._SetandAssembleForces()

        self.mstar, self.invmstar = self._ImplicitEuler()

        force = self.f.vec
        dif = self.gfu.vec.CreateVector()
        self.implicitform.Apply(self.gfu_old.vec, dif)

        rhs = self.gfu.vec.CreateVector()
        rhs.data = -dif + force

        increment = self.gfu.vec.CreateVector()
        increment.data = self.dt*self.invmstar*rhs
        return increment

    def _ComputeInnerProduct(self):
        """
        Assembles a Bilinear Form for the nonlinear diffusion.
        """

        self.innerproduct = BilinearForm(self.fes)
        dS = dx(element_boundary=True)



        diffusion =     self.u * self.v *dx +\
                        grad(self.u)*grad(self.v)*dx + \
                        (-grad(self.u)*self.n*self.jump_v - grad(self.v)*self.n*self.jump_u)*dS \
                        + self.alpha*(self.order+1)**2/self.h*self.jump_u*self.jump_v*dS

        #gradsquared = self.c_b2* grad(self.nu)*grad(self.u)* self.v*dx

        #self.implicitform += 1./self.sigma *(laplace + (1+self.c_b2)* gradsquared)
        self.innerproduct += diffusion
        with TaskManager():
            self.innerproduct.Assemble()
            self.invip = self.innerproduct.mat.Inverse(freedofs = self.fes.FreeDofs())


    def _SetArtificialDiffusion(self):

        self.artificial_diffusion = 0
        self._SetImplicitTerms()
        self._SetandAssembleForces()
        residual = self.gfu.vec.CreateVector()
        self.implicitform.Apply(self.gfu.vec, residual)
        #self.residualgfu.vec.data = residual - self.f.vec
        ###use vector norm of residual gfu/h**2
        ###use L2 projection, riesz representation, use residual as function

        self.residualgfu.vec.data = self.invip * (residual - self.f.vec)

        self.H1residualgfu.Set(self.residualgfu.components[0])
        C = 10**5
        self.artificial_diffusion = C * grad(self.residualgfu.components[0]) *grad(self.residualgfu.components[0]) * self.h**2
        self.artificialdiffusiongfu.components[0].Set(self.artificial_diffusion)
        #artificial_diffusion = 0

    def UpdateVisualization(self):

        """Interpolates CoefficientFunction for the eddyviscosity, the Production term and the distruction terms. Note that this is computationally expensive."""

        self.eddygfu.components[0].Set(CoefficientFunction(self.eddyviscosity))
        self.artificialdiffusiongfu.components[0].Set(self.artificial_diffusion)
        self.Dgfu.components[0].Set(self.nu_over_d**2)
        self.gridvorticity.components[0].Set(self.vort)

    def DoTimeStep(self,wind, vorticity, dt, iterations = 1):

        """
        Advances the simulation by one or more time steps

        Parameters
        ----------

        wind: ngsolve.fem.CoefficientFunction
            Current underlying velocityfield
        vorticity: ngsolve.fem.CoefficientFunction
            Current underlying vorticity
        dt: float
            time step
        iterations: int
            number of iterations of the solver without updating the underlying velocity field
        """
        self.dt = dt
        self.vortgfu.Set(wind)
        self.vort = grad(self.vortgfu)[1] - grad(self.vortgfu)[2]
        self.wind = self.vortgfu

        self._ComputeCoefficientFunctions()


        backup = self.gfu.vec.CreateVector()
        backup.data = self.gfu.vec

        #self._SetArtificialDiffusion()
        #Draw(self.artificial_diffusion, self.mesh, "artificial_diffusion")

        for i in range(iterations):
            self.gfu_old.vec.data = self.gfu.vec
            lastitvec = self.gfu.vec.CreateVector()
            lastitvec.data = self.gfu_old.vec
            itdiff = self.gfu.vec.CreateVector()
            it = 0
            print("substep iteration no.", i)
            for i in range(4):
                try:
                    increment = self._SetandComputeLinearSystem()
                    if(np.any(np.isnan(increment.FV().NumPy()))):
                        self.gfu.vec.data = backup
                        return False
                except netgen.libngpy._meshing.NgException or Exception:
                    self.gfu.vec.data = backup
                    return False

                self.gfu.vec.data = self.gfu_old.vec + increment
                # minor improvement to to avoid computing unnecessary steps
                if(np.any(self.gfu.components[0].vec.FV().NumPy() < -0.1)):
                    print("negative Diffusion")
                    self.gfu.vec.data = backup
                    return False
                itdiff.data = lastitvec - self.gfu.vec

                itdiffnorm = Norm(itdiff)/len(itdiff)/dt
                print("fix point iteration no.", it, ", itdiffnorm:", itdiffnorm)
                if itdiffnorm < self.PicardTOL:
                    break

                if it > 0 and itdiffnormlast < itdiffnorm * 1.05:
                    print("fix point iteration diverges or converging too slowly. I am rejecting this step")
                    self.gfu.vec.data = backup
                    return False
                itdiffnormlast = itdiffnorm
                lastitvec.data = self.gfu.vec
                it += 1
                Redraw()

        self._ComputeEddyViscosity()

        Redraw()
        return True

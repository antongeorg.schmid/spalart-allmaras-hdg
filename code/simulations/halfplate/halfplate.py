import time
from ngsolve import *
from netgen.geom2d import SplineGeometry
import sys
import os
sys.path.append(os.getcwd())
sys.path.append('../../')
sys.path.append('../utils/')
import pickle
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from eikonalsolver.eikonalsolver import eikonalsolver

from RANSclosure.SpalartAllmarasHDG import SpalartAllmarasHDG
from DNS.NavierStokesHDG import NavierStokesHDG
from geometry.geometry import MakeInfiniteHalfPlate
from geometry.geometry import ImportstructuredPlot3DHalfPlate2D
SAsolver = SpalartAllmarasHDG
# Background viscosity and freestream viscosity for the working variable

mesh = ImportstructuredPlot3DHalfPlate2D("../utils/geometry/meshes/flatplate_clust2_4levelsdown_35x25.p2dfmt", nx = 35, ny = 25)

distance = IfPos(x, y, (x**2 + y**2)**0.5)
walldistance = distance
Draw(mesh)
#mesh2 = Mesh( geo.GenerateMesh(maxh = 1.0) ); Draw(mesh2)
uin = CoefficientFunction(( 1, 0))

t = 0
tend = 10
tau = 0.001
substeps = 10
nu = 1e-5
tau_w = 0

NavierStokes = NavierStokesHDG(mesh = mesh, order = 3, quads = True, nu_0 = nu, uin = uin, inlet = "left", dirichlet = "plate", neumann = "freestream|top", tau_w = tau_w)

### Spalart-Allmaras setup
order = 3
epsilon = 1e-24
PicardTOL = 1e-8
nuhat = SAsolver(mesh = mesh, inlet = "left" , dirichlet = "inlet|plate", walldistance = walldistance, order = order ,  nu_0 = nu, inflow = 3*nu, initialfield = 3*nu, epsilon = epsilon, PicardTOL = PicardTOL)

#input("")

nu_t = 0
subdiv_sa = 1

nuold = nuhat.gfu.vec.CreateVector()
uold = NavierStokes.gfu.components[0].vec.CreateVector()
timestepcounter = 0

velfilename = "simulationdata/velocitygfu_35x25halflateRe=1e5.pickle"
viscfilename = "simulationdata/viscositygfu_35x25halflateRe=1e5.pickle"

### get better initial conditions before computing the eddy-viscosity
NavierStokes.DoRANSiteration(tau = 0.05, iterations = 500, nu_t = 0)

while(t<10):
    with TaskManager():
        uold.data = NavierStokes.gfu.components[0].vec
        NavierStokes.DoRANSiteration(tau = 0.005, iterations = 200, nu_t = nu_t)
        uold.data -= NavierStokes.gfu.components[0].vec
        uold.data *= 1.0/(0.05*25)

        wind = NavierStokes.gfu.components[0]
        vorticity = grad(wind)[1] - grad(wind)[2]

        ### time step management for the Spalart-Allmaras solver
        nuold.data = nuhat.gfu.vec
        print("Norm(nuhat) = {}".format(Norm(nuold)))
        accepted_timestep = False
        while not accepted_timestep:
            accepted_timestep = nuhat.DoTimeStep(wind = wind, vorticity = vorticity, dt = tau/subdiv_sa, iterations = subdiv_sa)
            if not accepted_timestep:
                subdiv_sa *= 2
                print("changed dt_sa to", tau/subdiv_sa)

        nuold.data -= nuhat.gfu.vec
        nuold.data *= 1.0/tau
        print("dnu/tau ~, dv/dtau ~ ",Norm(nuold), Norm(uold))


        nu_t = nuhat.eddyviscosity
        t += tau
        print ("\r  t =", t, end="")

        if(Norm(nuold) < 1e-7 and Norm(uold)< 1e-6):
                print("Succes")
                break
        Redraw(blocking=True)

        subdiv_sa = subdiv_sa//2
        if(subdiv_sa == 0):
            subdiv_sa = 1
            if(tau<0.01):
                tau = 2*tau
        print("succesful timestep, try dtsa ={} next".format(tau/subdiv_sa))

        ### export data using pickle
        velfilehandler = open(velfilename, "wb")
        pickle.dump(NavierStokes.gfu, velfilehandler)
        velfilehandler.close()
        viscfilehandler = open(viscfilename, "wb")
        pickle.dump(nuhat.gfu, viscfilehandler)
        viscfilehandler.close()

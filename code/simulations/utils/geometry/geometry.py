#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ngsolve import *
from netgen.geom2d import SplineGeometry
from netgen.meshing import *
from netgen.csg import *

class recgeometry():
    """
    Returns a rectangular channel geometry and a coefficient function for the wall distance, as well as a parabolic inflow law for the Navier Stokes equations.

    Returns
    -------
    geo: netgen.libngpy._geom2d.SplineGeometry
    walldistance: ngsolve.fem.CoefficientFunction
    uin: ngsolve.fem.CoefficientFunction

    """
    def __init__(self):
        self.geo = SplineGeometry()
        h = 0.41
        self.geo.AddRectangle( (0, 0), (2,h), bcs = ("wall", "outlet", "wall", "inlet"))
        self.uin = CoefficientFunction( (1.5*4*y*(h-y)/(h*h), 0) )
        self.walldistance = IfPos(y-h/2., h-y, y)


class stepgeometry():
    """
    Returns a channel geometry with a backward facing step and a coefficient function for the wall distance, as well as a parabolic inflow law for the Navier Stokes equations.

    Returns
    -------
    geo: netgen.libngpy._geom2d.SplineGeometry
    walldistance: ngsolve.fem.CoefficientFunction
    uin: ngsolve.fem.CoefficientFunction

    """
    def __init__(self):
        self.geo = SplineGeometry()
        #geo.AddRectangle( (0, 0), (2, 0.41), bcs = ("wall", "outlet", "wall", "inlet"))

        totheight = 0.41
        h = totheight
        steplength = 0.7
        stepheight = 0.05
        b = steplength
        a = stepheight

        maxh = 0.08
        localmaxh = 0.005
        pts = [(0.0, a, localmaxh), (b,a, localmaxh), (b, 0., localmaxh), (2,0, maxh), (2,h, maxh), (0,h, localmaxh)]
        p1, p2, p3, p4, p5, p6 = [self.geo.AppendPoint(*pt) for pt in pts]

        curves = [[["line",p1,p2],"wall"],
                  [["line",p3,p4],"wall"],
                  [["line",p4,p5],"outlet"],
                  [["line",p5,p6],"wall"],
                  [["line",p6,p1],"inlet"]]
        [self.geo.Append(c,bc = bc) for c,bc in curves]

        wallcurves = [
                  [["line",p2,p3],"wall"]]
        [self.geo.Append(c, bc = bc, maxh = localmaxh) for c, bc in wallcurves]

        self.walldistance = IfPos(x - b, IfPos(y-h/2., h-y, IfPos(x-b-a, y, IfPos(y-a, IfPos(y-a - (x-b), y-a, x-b), IfPos(y-(x-b), x-b, y)))), IfPos(y - 0.5*(h+a), h-y, y-a))

        self.uin = CoefficientFunction( (1.5*4*(y-a)*(h-y)/((h-a)*(h-a)), 0) )

class periodic_Channel_triag():
    def __init__(self):
        h = 0.41
        L = 2
        self.geo = SplineGeometry()
        pts = [(0,0), (L,0), (L,h), (0,h)]
        p1, p2, p3, p4 = [self.geo.AppendPoint(*pt) for pt in pts]
        lowerwall = ["line", p1, p2]
        rightperiodic = ["line", p3, p2]
        upperwall = ["line", p3, p4]
        leftperiodic = ["line", p4, p1]

        self.geo.Append(lowerwall, bc = "wall")
        self.geo.Append(upperwall, bc = "wall")
        lhboundary = self.geo.Append(leftperiodic)
        self.geo.Append(rightperiodic, copy = lhboundary, leftdomain = 0, rightdomain = 1)
        self.mesh = Mesh(self.geo.GenerateMesh(maxh = 0.07))
        self.walldistance = IfPos(y-h/2., h-y, y)

def MakeInfiniteHalfPlate(quads=True, nx_free = 5, nx_plate = 5, ny = 10, secondorder=False, periodic_x=False, periodic_y=False, mapping = None, bbpts=None, bbnames=None, flip_triangles=False):
    """
    Generate a structured 2D mesh

    Parameters
    ----------
    quads : bool
      If True, a quadrilateral mesh is generated. If False, the quads are split to triangles.

    nx : int
      Number of cells in x-direction.

    ny : int
      Number of cells in y-direction.

    secondorder : bool
      If True, second order curved elements are used.

    periodic_x: bool
      If True, the left and right boundaries are identified to generate a periodic mesh in x-direction.

    periodic_y: bool
      If True, the top and bottom boundaries are identified to generate a periodic mesh in y-direction.

    mapping: lamda
      Mapping to transform the generated points. If None, the identity mapping is used.

    bbpts : list
      List of points which should be handled as BBND and are named with bbnames. The mesh (nx, ny and mapping) must be constructed in such a way that the bbpts coincide with generated points. Otherwise an Exception is thrown.

    bbnames : list
      List of bbnd names as strings. Size must coincide with size of bbpts. Otherwise an Exception is thrown.

    flip_triangles : bool
      If set tot True together with quads=False the quads are cut the other way round

    Returns
    -------
    (ngsolve.mesh)
      Returns generated 2D NGSolve mesh

    """
    mesh = Mesh()
    mesh.dim=2
    nx = nx_free +nx_plate

    if (bbpts and bbnames) and len(bbpts) != len(bbnames):
        raise Exception("Lenght of bbnames does not coincide with length of bbpts!")

    found = []
    indbbpts = []
    if bbpts:
        for i in range(len(bbpts)):
            found.append(False)
            indbbpts.append(None)

    pids = []
    if periodic_y:
        minioni = []
        masteri = []
    if periodic_x:
        minionj = []
        masterj = []
    for i in range(ny+1):
        for j in range(nx_free+ nx_plate+1):
            x,y = j/nx, i/ny
            # if mapping:
            #    x,y = mapping(x,y)
            pids.append(mesh.Add (MeshPoint(Pnt(x,y,0))))

    # mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))
    idx_dom = mesh.AddRegion("dom", dim=2)
    idx_free = mesh.AddRegion("freestream", dim=1)
    idx_plate = mesh.AddRegion("plate", dim=1)
    idx_right  = mesh.AddRegion("right", dim=1)
    idx_top    = mesh.AddRegion("top", dim=1)
    idx_left   = mesh.AddRegion("left", dim=1)


    for i in range(ny):
        for j in range(nx):
            base = i * (nx+1) + j
            if quads:
                pnum = [base,base+1,base+nx+2,base+nx+1]
                elpids = [pids[p] for p in pnum]
                el = Element2D(idx_dom,elpids)
                if not mapping:
                    el.curved=False
                mesh.Add(el)
            else:
                if flip_triangles:
                    pnum1 = [base,base+1,base+nx+2]
                    pnum2 = [base,base+nx+2,base+nx+1]
                else:
                    pnum1 = [base,base+1,base+nx+1]
                    pnum2 = [base+1,base+nx+2,base+nx+1]
                elpids1 = [pids[p] for p in pnum1]
                elpids2 = [pids[p] for p in pnum2]
                mesh.Add(Element2D(idx_dom,elpids1))
                mesh.Add(Element2D(idx_dom,elpids2))

    for i in range(nx_free):
        mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_free))
    for i in range(nx_free,nx_free + nx_plate):
        mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_plate))
    for i in range(ny):
        mesh.Add(Element1D([pids[i*(nx+1)+nx], pids[(i+1)*(nx+1)+nx]], index=idx_right))
    for i in range(nx):
        mesh.Add(Element1D([pids[ny*(nx+1)+i+1], pids[ny*(nx+1)+i]], index=idx_top))
    for i in range(ny):
        mesh.Add(Element1D([pids[(i+1)*(nx+1)], pids[i*(nx+1)]], index=idx_left))

    mesh.SetBCName(0, "freestream")
    mesh.SetBCName(1, "plate")
    mesh.SetBCName(2, "right")
    mesh.SetBCName(3, "top")
    mesh.SetBCName(4, "left")
    mesh.Compress()

    if secondorder:
        mesh.SecondOrder()

    if mapping:
        for p in mesh.Points():
            x,y,z = p.p
            x,y = mapping(x,y)
            p[0] = x
            p[1] = y

    import ngsolve
    ngsmesh = ngsolve.Mesh(mesh)   ##Mesh seems to be overloaded
    return ngsmesh



def ImportstructuredPlot3DHalfPlate2D(file = None, nx = 10, ny = 10, x_axis = None, y_axis = 1, periodic_x=False, periodic_y=False):
    if(file != None):
        import numpy as np
        data = np.genfromtxt(file, skip_header = 2, skip_footer = 1)
        data = data.flatten()
        x_vals = data[:nx]
        y_vals = data[nx*ny::nx]
    else:
        x_vals = x_axis
        y_vals = y_axis

    print(x_vals)
    print(y_vals)

    ### nx, ny is the number of sections, thats why we reduce it by one.
    nx = nx -1
    ny = ny -1

    mesh = Mesh()
    mesh.dim=2

    # found = []
    # indbbpts = []
    # if bbpts:
    #     for i in range(len(bbpts)):
    #         found.append(False)
    #         indbbpts.append(None)

    pids = []
    if periodic_y:
        minioni = []
        masteri = []
    if periodic_x:
        minionj = []
        masterj = []
    for i in range(ny+1):
        for j in range(nx+1):
            x,y = x_vals[j], y_vals[i]
            # if mapping:
            #    x,y = mapping(x,y)
            pids.append(mesh.Add (MeshPoint(Pnt(x,y,0))))

    # mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))
    idx_dom = mesh.AddRegion("dom", dim=2)
    idx_free = mesh.AddRegion("freestream", dim=1)
    idx_plate = mesh.AddRegion("plate", dim=1)
    idx_right  = mesh.AddRegion("right", dim=1)
    idx_top    = mesh.AddRegion("top", dim=1)
    idx_left   = mesh.AddRegion("left", dim=1)


    for i in range(ny):
        for j in range(nx):
            base = i * (nx+1) + j
            pnum = [base,base+1,base+nx+2,base+nx+1]
            elpids = [pids[p] for p in pnum]
            el = Element2D(idx_dom,elpids)
            mesh.Add(el)


    for i in range(nx):
        if(x_vals[i] < 0):
            mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_free))
        else:
            mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_plate))
    for i in range(ny):
        mesh.Add(Element1D([pids[i*(nx+1)+nx], pids[(i+1)*(nx+1)+nx]], index=idx_right))
    for i in range(nx):
        mesh.Add(Element1D([pids[ny*(nx+1)+i+1], pids[ny*(nx+1)+i]], index=idx_top))
    for i in range(ny):
        mesh.Add(Element1D([pids[(i+1)*(nx+1)], pids[i*(nx+1)]], index=idx_left))

    mesh.SetBCName(0, "freestream")
    mesh.SetBCName(1, "plate")
    mesh.SetBCName(2, "right")
    mesh.SetBCName(3, "top")
    mesh.SetBCName(4, "left")
    mesh.Compress()

    """
        if mapping:
            for p in mesh.Points():
                x,y,z = p.p
                x,y = mapping(x,y)
                p[0] = x
                p[1] = y
    """
    import ngsolve
    ngsmesh = ngsolve.Mesh(mesh)   ##Mesh seems to be overloaded
    return ngsmesh



def MakePeriodicHalfChannel(secondorder=False, periodic_x=True, periodic_y=False, mapping = None, bbpts=None, bbnames=None, flip_triangles=False):
    """
    Generate a structured 2D mesh

    Parameters
    ----------
    quads : bool
      If True, a quadrilateral mesh is generated. If False, the quads are split to triangles.

    nx : int
      Number of cells in x-direction.

    ny : int
      Number of cells in y-direction.

    secondorder : bool
      If True, second order curved elements are used.

    periodic_x: bool
      If True, the left and right boundaries are identified to generate a periodic mesh in x-direction.

    periodic_y: bool
      If True, the top and bottom boundaries are identified to generate a periodic mesh in y-direction.

    mapping: lamda
      Mapping to transform the generated points. If None, the identity mapping is used.

    bbpts : list
      List of points which should be handled as BBND and are named with bbnames. The mesh (nx, ny and mapping) must be constructed in such a way that the bbpts coincide with generated points. Otherwise an Exception is thrown.

    bbnames : list
      List of bbnd names as strings. Size must coincide with size of bbpts. Otherwise an Exception is thrown.

    flip_triangles : bool
      If set tot True together with quads=False the quads are cut the other way round

    Returns
    -------
    (ngsolve.mesh)
      Returns generated 2D NGSolve mesh

    """
    mesh = Mesh()
    mesh.dim=2
    nx_vals = [i*0.2 for i in range(11)]
    ny_vals = [0.00000000e+00, 8.32003352e-06, 1.86610410e-05, 3.35349003e-05,
 5.65545393e-05, 9.33115332e-05, 1.52734323e-04, 2.49256974e-04,
 4.06325279e-04, 6.62091841e-04, 1.07868352e-03, 1.75729234e-03,
 2.86275546e-03, 4.66359490e-03, 7.59724275e-03, 1.23762954e-02,
 2.01616066e-02, 3.28442636e-02, 5.35049417e-02, 8.71622135e-02,
 1.41991583e-01, 2.31311355e-01, 3.76817706e-01, 6.13854793e-01,
 1.00000000e+00]
    nx = 10
    ny = 24
    """
    if (bbpts and bbnames) and len(bbpts) != len(bbnames):
        raise Exception("Lenght of bbnames does not coincide with length of bbpts!")

    found = []
    indbbpts = []
    if bbpts:
        for i in range(len(bbpts)):
            found.append(False)
            indbbpts.append(None)
    """
    pids = []
    if periodic_y:
        minioni = []
        masteri = []
    if periodic_x:
        minionj = []
        masterj = []
    for i in range(ny+1):
        for j in range(nx+1):
            x,y = nx_vals[j], ny_vals[i]
            # if mapping:
            #    x,y = mapping(x,y)
            pids.append(mesh.Add (MeshPoint(Pnt(x,y,0))))
            if periodic_x:
                if j == 0:
                    minionj.append(pids[-1])
                if j == nx:
                    masterj.append(pids[-1])

    if periodic_x:
        for j in range(len(minionj)):
            mesh.AddPointIdentification(masterj[j],minionj[j],identnr=2,type=2)

    # mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))
    idx_dom = mesh.AddRegion("dom", dim=2)
    idx_wall = mesh.AddRegion("plate", dim=1)
    idx_right  = mesh.AddRegion("right", dim=1)
    idx_top = mesh.AddRegion("top", dim=1)
    idx_left   = mesh.AddRegion("left", dim=1)


    for i in range(ny):
        for j in range(nx):
            base = i * (nx+1) + j
            pnum = [base,base+1,base+1+nx+1,base+nx+1]
            elpids = [pids[p] for p in pnum]
            el = Element2D(idx_dom,elpids)
            mesh.Add(el)

    for i in range(nx):
        mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_wall))
    for i in range(ny):
        mesh.Add(Element1D([pids[i*(nx+1)+nx], pids[(i+1)*(nx+1)+nx]], index=idx_right))
    for i in range(nx):
        mesh.Add(Element1D([pids[ny*(nx+1)+i+1], pids[ny*(nx+1)+i]], index=idx_top))
    for i in range(ny):
        mesh.Add(Element1D([pids[(i+1)*(nx+1)], pids[i*(nx+1)]], index=idx_left))


    mesh.SetBCName(0, "wall")
    mesh.SetBCName(1, "right")
    mesh.SetBCName(2, "top")
    mesh.SetBCName(3, "left")
    mesh.Compress()

    if secondorder:
        mesh.SecondOrder()

    if mapping:
        for p in mesh.Points():
            x,y,z = p.p
            x,y = mapping(x,y)
            p[0] = x
            p[1] = y

    import ngsolve
    ngsmesh = ngsolve.Mesh(mesh)
    return ngsmesh


def MakePeriodicHalfChannel49(secondorder=False, periodic_x=True, periodic_y=False, mapping = None, bbpts=None, bbnames=None, flip_triangles=False):
    """
    Generate a structured 2D mesh

    Parameters
    ----------
    quads : bool
      If True, a quadrilateral mesh is generated. If False, the quads are split to triangles.

    nx : int
      Number of cells in x-direction.

    ny : int
      Number of cells in y-direction.

    secondorder : bool
      If True, second order curved elements are used.

    periodic_x: bool
      If True, the left and right boundaries are identified to generate a periodic mesh in x-direction.

    periodic_y: bool
      If True, the top and bottom boundaries are identified to generate a periodic mesh in y-direction.

    mapping: lamda
      Mapping to transform the generated points. If None, the identity mapping is used.

    bbpts : list
      List of points which should be handled as BBND and are named with bbnames. The mesh (nx, ny and mapping) must be constructed in such a way that the bbpts coincide with generated points. Otherwise an Exception is thrown.

    bbnames : list
      List of bbnd names as strings. Size must coincide with size of bbpts. Otherwise an Exception is thrown.

    flip_triangles : bool
      If set tot True together with quads=False the quads are cut the other way round

    Returns
    -------
    (ngsolve.mesh)
      Returns generated 2D NGSolve mesh

    """
    mesh = Mesh()
    mesh.dim=2
    nx_vals = [i*0.2 for i in range(11)]
    ny_vals = [0.0, 4.03918221137e-06, 8.32003351792e-06, 1.30986823652e-05, 1.86610410204e-05, 2.5339912044e-05, 3.35349002651e-05, 4.37363216153e-05, 5.65545393156e-05, 7.27564826386e-05, 9.33115332007e-05, 0.000119449524223, 0.000152734322921, 0.000195157398549, 0.000249256974381, 0.000318269892643, 0.000406325278647, 0.00051869159129, 0.000662091841241, 0.000845105836695, 0.00107868352355, 0.00137680013374, 0.00175729234005, 0.00224292544556, 0.00286275545888, 0.0036538675499, 0.00466359489973, 0.00595235070148, 0.00759724275377, 0.00969668691233, 0.0123762954283, 0.0157963924789, 0.020161606555, 0.0257331136275, 0.0328442636177, 0.0419205251215, 0.0535049417031, 0.0682906228403, 0.0871622134919, 0.111248823462, 0.141991583389, 0.181229869311, 0.231311354732, 0.295232474728, 0.376817706242, 0.480948391101, 0.61385479253, 0.783488859235, 1.0
    ]
    nx = 10
    ny = 48
    """
    if (bbpts and bbnames) and len(bbpts) != len(bbnames):
        raise Exception("Lenght of bbnames does not coincide with length of bbpts!")

    found = []
    indbbpts = []
    if bbpts:
        for i in range(len(bbpts)):
            found.append(False)
            indbbpts.append(None)
    """
    pids = []
    if periodic_y:
        minioni = []
        masteri = []
    if periodic_x:
        minionj = []
        masterj = []
    for i in range(ny+1):
        for j in range(nx+1):
            x,y = nx_vals[j], ny_vals[i]
            # if mapping:
            #    x,y = mapping(x,y)
            pids.append(mesh.Add (MeshPoint(Pnt(x,y,0))))
            if periodic_x:
                if j == 0:
                    minionj.append(pids[-1])
                if j == nx:
                    masterj.append(pids[-1])

    if periodic_x:
        for j in range(len(minionj)):
            mesh.AddPointIdentification(masterj[j],minionj[j],identnr=2,type=2)

    # mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))
    idx_dom = mesh.AddRegion("dom", dim=2)
    idx_wall = mesh.AddRegion("plate", dim=1)
    idx_right  = mesh.AddRegion("right", dim=1)
    idx_top = mesh.AddRegion("top", dim=1)
    idx_left   = mesh.AddRegion("left", dim=1)


    for i in range(ny):
        for j in range(nx):
            base = i * (nx+1) + j
            pnum = [base,base+1,base+1+nx+1,base+nx+1]
            elpids = [pids[p] for p in pnum]
            el = Element2D(idx_dom,elpids)
            mesh.Add(el)

    for i in range(nx):
        mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_wall))
    for i in range(ny):
        mesh.Add(Element1D([pids[i*(nx+1)+nx], pids[(i+1)*(nx+1)+nx]], index=idx_right))
    for i in range(nx):
        mesh.Add(Element1D([pids[ny*(nx+1)+i+1], pids[ny*(nx+1)+i]], index=idx_top))
    for i in range(ny):
        mesh.Add(Element1D([pids[(i+1)*(nx+1)], pids[i*(nx+1)]], index=idx_left))


    mesh.SetBCName(0, "wall")
    mesh.SetBCName(1, "right")
    mesh.SetBCName(2, "top")
    mesh.SetBCName(3, "left")
    mesh.Compress()

    if secondorder:
        mesh.SecondOrder()

    if mapping:
        for p in mesh.Points():
            x,y,z = p.p
            x,y = mapping(x,y)
            p[0] = x
            p[1] = y

    import ngsolve
    ngsmesh = ngsolve.Mesh(mesh)
    return ngsmesh


def MakePeriodicHalfChannel97(secondorder=False, periodic_x=True, periodic_y=False, mapping = None, bbpts=None, bbnames=None, flip_triangles=False):
    """
    Generate a structured 2D mesh

    Parameters
    ----------
    quads : bool
      If True, a quadrilateral mesh is generated. If False, the quads are split to triangles.

    nx : int
      Number of cells in x-direction.

    ny : int
      Number of cells in y-direction.

    secondorder : bool
      If True, second order curved elements are used.

    periodic_x: bool
      If True, the left and right boundaries are identified to generate a periodic mesh in x-direction.

    periodic_y: bool
      If True, the top and bottom boundaries are identified to generate a periodic mesh in y-direction.

    mapping: lamda
      Mapping to transform the generated points. If None, the identity mapping is used.

    bbpts : list
      List of points which should be handled as BBND and are named with bbnames. The mesh (nx, ny and mapping) must be constructed in such a way that the bbpts coincide with generated points. Otherwise an Exception is thrown.

    bbnames : list
      List of bbnd names as strings. Size must coincide with size of bbpts. Otherwise an Exception is thrown.

    flip_triangles : bool
      If set tot True together with quads=False the quads are cut the other way round

    Returns
    -------
    (ngsolve.mesh)
      Returns generated 2D NGSolve mesh

    """
    mesh = Mesh()
    mesh.dim=2
    nx_vals = [i*0.2 for i in range(11)]
    ny_vals = [0.0, 2.00465414801e-06, 4.03918221136e-06, 6.13390329527e-06, 8.32003351787e-06, 1.06301512017e-05, 1.30986823651e-05, 1.57624137484e-05, 1.86610410203e-05, 2.18377603328e-05, 2.53399120438e-05, 2.92196861955e-05, 3.35349002649e-05, 3.83498607765e-05, 4.37363216149e-05, 4.97745533233e-05, 5.65545393152e-05, 6.41773168342e-05, 7.27564826381e-05, 8.24198858494e-05, 9.33115332e-05, 0.000105593735057, 0.000119449524222, 0.00013508538353, 0.00015273432292, 0.000172659351812, 0.000195157398548, 0.000220563735297, 0.000249256974379, 0.000281664710452, 0.000318269892641, 0.000359618021586, 0.000406325278645, 0.000459087708394, 0.000518691591286, 0.000586025161018, 0.000662091841236, 0.000748025198834, 0.00084510583669, 0.000954780477558, 0.00107868352354, 0.00121866141241, 0.00137680013373, 0.00155545631485, 0.00175729234004, 0.00198531602607, 0.00224292544554, 0.00253395956592, 0.00286275545886, 0.00323421293254, 0.00365386754988, 0.00412797312109, 0.0046635948997, 0.00526871487093, 0.00595235070145, 0.00672469012318, 0.00759724275372, 0.00858301161586, 0.00969668691228, 0.010954864943, 0.0123762954282, 0.0139821609216, 0.0157963924788, 0.017846026285, 0.0201616065549, 0.022777640712, 0.0257331136274, 0.029072068583, 0.0328442636176, 0.0371059130341, 0.0419205251213, 0.0473598485723, 0.0535049417029, 0.0604473804058, 0.06829062284, 0.0771515511929, 0.0871622134916, 0.0984717914192, 0.111248823462, 0.125683716517, 0.141991583389, 0.160415448461, 0.181229869311, 0.204745028247, 0.231311354731, 0.261324747572, 0.295232474727, 0.333539838608, 0.376817706242, 0.425711016488, 0.480948391101, 0.543352992851, 0.613854792529, 0.693504427627, 0.783488859235, 0.885149060463, 1.0
]
    nx = 10
    ny = 96
    """
    if (bbpts and bbnames) and len(bbpts) != len(bbnames):
        raise Exception("Lenght of bbnames does not coincide with length of bbpts!")

    found = []
    indbbpts = []
    if bbpts:
        for i in range(len(bbpts)):
            found.append(False)
            indbbpts.append(None)
    """
    pids = []
    if periodic_y:
        minioni = []
        masteri = []
    if periodic_x:
        minionj = []
        masterj = []
    for i in range(ny+1):
        for j in range(nx+1):
            x,y = nx_vals[j], ny_vals[i]
            # if mapping:
            #    x,y = mapping(x,y)
            pids.append(mesh.Add (MeshPoint(Pnt(x,y,0))))
            if periodic_x:
                if j == 0:
                    minionj.append(pids[-1])
                if j == nx:
                    masterj.append(pids[-1])

    if periodic_x:
        for j in range(len(minionj)):
            mesh.AddPointIdentification(masterj[j],minionj[j],identnr=2,type=2)

    # mesh.Add(FaceDescriptor(surfnr=1,domin=1,bc=1))
    idx_dom = mesh.AddRegion("dom", dim=2)
    idx_wall = mesh.AddRegion("plate", dim=1)
    idx_right  = mesh.AddRegion("right", dim=1)
    idx_top = mesh.AddRegion("top", dim=1)
    idx_left   = mesh.AddRegion("left", dim=1)


    for i in range(ny):
        for j in range(nx):
            base = i * (nx+1) + j
            pnum = [base,base+1,base+1+nx+1,base+nx+1]
            elpids = [pids[p] for p in pnum]
            el = Element2D(idx_dom,elpids)
            mesh.Add(el)

    for i in range(nx):
        mesh.Add(Element1D([pids[i], pids[i+1]], index=idx_wall))
    for i in range(ny):
        mesh.Add(Element1D([pids[i*(nx+1)+nx], pids[(i+1)*(nx+1)+nx]], index=idx_right))
    for i in range(nx):
        mesh.Add(Element1D([pids[ny*(nx+1)+i+1], pids[ny*(nx+1)+i]], index=idx_top))
    for i in range(ny):
        mesh.Add(Element1D([pids[(i+1)*(nx+1)], pids[i*(nx+1)]], index=idx_left))


    mesh.SetBCName(0, "wall")
    mesh.SetBCName(1, "right")
    mesh.SetBCName(2, "top")
    mesh.SetBCName(3, "left")
    mesh.Compress()

    if secondorder:
        mesh.SecondOrder()

    if mapping:
        for p in mesh.Points():
            x,y,z = p.p
            x,y = mapping(x,y)
            p[0] = x
            p[1] = y

    import ngsolve
    ngsmesh = ngsolve.Mesh(mesh)
    return ngsmesh

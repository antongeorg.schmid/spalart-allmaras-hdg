#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
#sys.path.append(os.getcwd())
sys.path.append("../")

from ngsolve import *
from ngsolve.meshes import *
from netgen.geom2d import SplineGeometry

from geometry.geometry import recgeometry
from geometry.geometry import stepgeometry
from geometry.geometry import periodic_Channel_triag
from geometry.geometry import MakeInfiniteHalfPlate
from geometry.geometry import ImportstructuredPlot3DHalfPlate2D
from geometry.geometry import MakePeriodicHalfChannel
from geometry.eikonalsolver import eikonalsolver

mesh = MakeInfiniteHalfPlate(nx_plate = 20)

distance = eikonalsolver(mesh = mesh, order = 3, diffusioncoefficient = 0.1, BND = "plate")

distance.DoIteration()
TOL = 1e-3

while(distance.diff > TOL):
    distance.DoIteration()

import time
from ngsolve import *
from netgen.geom2d import SplineGeometry
import sys
import os
sys.path.append(os.getcwd())
sys.path.append('../../')
sys.path.append('../')
import pickle

from RANSclosure.SpalartAllmarasHDG import SpalartAllmarasHDG
from DNS.NavierStokesHDG import NavierStokesHDG
from utils.geometry.geometry import *
import numpy as np
SAsolver = SpalartAllmarasHDG

mesh = MakePeriodicHalfChannel49()
distancefunction = y
#Set backgroud viscosity

#SpalartAllmaras setup

nu_0 = 8e-6
nu = CoefficientFunction(nu_0)
nu_t = 0
order = 3
eps = 1e-12
PicardTOL = 1e-8
nuhat = SAsolver(mesh, walldistance = distancefunction, inlet = "" , inflow = None , dirichlet = "wall", order = order, nu_0 = nu_0, initialfield = 0.02, epsilon = eps, PicardTOL = PicardTOL)

### Import of reference data as initial conditions
# u_tau_0550 = 5.43496e-02
# u_tau_5200 = 4.14872e-02
# u_tau = u_tau_5200
# U_Moser = CoefficientFunction((u_tau * MoserdataCF("../run/externalData/LM_Channel_5200_mean_prof.preprocessedcsv"), 0))

### RANS setup
U = CoefficientFunction(((3/2.)*y*(2-y), 0))
order = 3
NavierStokes = NavierStokesHDG(mesh = mesh, order = order, uin = None, uinitial = U, quads = True, nu_0 = nu_0, dirichlet = "wall", tau_w = 0, ComputeStationary = False)
wind = NavierStokes.gfu.components[0]

### Computation of average velocity. It is kept constant at roughly 1 by adjusting the pressure gradient.
targetbulkvelocity = 1
def averagevelocity():
    samples = 10000
    y_vals = np.linspace(0,1, samples)
    U = [NavierStokes.gfu.components[0](mesh(1,y_val)) for y_val in y_vals]
    U_x = [x for (x,y) in U]
    average = np.average(U_x)
    print("averagevelocity ={}".format(average))
    return average

TOL = 1e-6
adaptationparameter = 1e-2
initialbulkvelocity = averagevelocity()


Redraw()
input("")
### backup gridfunctions
oldgfuvec = NavierStokes.gfu.vec.CreateVector()
nuold = nuhat.gfu.vec.CreateVector()

#outputfiles
velocityoutputfile = "simulationdata/periodicChannelvelocityRe5200_49eps1e-12.pickle"
viscosityoutputfile = "simulationdata/periodicChannelviscosityRe5200_49eps1e-12.pickle"

#overall timestepping

tau = 0.01  #will be adjusted by SpalartAllmaras timestepping
subdiv_sa = 1
tau_vel = 0.0005  #keep this fixed to ensure stability of NavierStokes part
t = 0
tend = 100

with TaskManager():
    while t < tend:    #use for loop

        # velocityfilehandler = open(velocityoutputfile, "wb")
        # pickle.dump(NavierStokes.gfu, velocityfilehandler)
        # velocityfilehandler.close()
        #
        # viscosityfilehandler = open(viscosityoutputfile, "wb")
        # pickle.dump(nuhat.gfu, viscosityfilehandler)
        # velocityfilehandler.close()

        pressure_upper_bound = 0.0015  #bounds set empirically based on test runs
        pressure_lower_bound = 0.0013
        oldgfuvec.data = NavierStokes.gfu.vec
        oldbulkvelocity = averagevelocity()

        if(oldbulkvelocity < targetbulkvelocity):
            guess = pressure_upper_bound
        else:
            guess = pressure_lower_bound

        pressure = guess
        pressure_gradient = (pressure, 0)
        print("pressure_gradient = ~",pressure_gradient)
        iter = 500 # fixes iterations of the RANS equations before updating the eddyviscosity
        NavierStokes.DoRANSiteration(tau = tau_vel, iterations = iter, nu_t = nu_t, pressure_gradient = CoefficientFunction(pressure_gradient))

        vorticity = grad(wind)[1] - grad(wind)[2]

        #SpalartAllmaras time step

        nuold.data = nuhat.gfu.vec
        accepted_timestep = False
        while not accepted_timestep:
            accepted_timestep = nuhat.DoTimeStep(wind = CoefficientFunction(NavierStokes.gfu.components[0]), vorticity = vorticity, dt = tau/subdiv_sa, iterations = subdiv_sa)
            if not accepted_timestep:
                subdiv_sa *= 2
                print("changed tau_sa to", tau/subdiv_sa)

        #compute current derivatives

        normnu = Norm(nuold)
        nuold.data -= nuhat.gfu.vec
        nuold.data *= 1.0/tau
        dnudt = Norm(nuold)
        print("  ---  nu ~ ", normnu)
        print("  ---  dnu/dt ~ ",dnudt)

        #Spalart-Allmaras time step management

        subdiv_sa = subdiv_sa//2
        if(subdiv_sa == 0):
            subdiv_sa = 1
            if(tau<0.1):
                tau = 2*tau
        print("succesful timestep, try tausa ={} next".format(tau/subdiv_sa))

        ## stopping criterion

        if(dnudt < 0.001):
            print("Success")
            break
        nu_t = nuhat.eddyviscosity
        t = t + tau
        print("t:",t)
# =============================================================================
#         counter +=1
#         if(counter %1 ==0):
#             vtk.Do()
# =============================================================================
        Redraw()

### Export data using pickle

velocityfilehandler = open(velocityoutputfile, "wb")
pickle.dump(NavierStokes.gfu, velocityfilehandler)
velocityfilehandler.close()

viscosityfilehandler = open(viscosityoutputfile, "wb")
pickle.dump(nuhat.gfu, viscosityfilehandler)
velocityfilehandler.close()

#README

In this project, we provide an initial implementation of the Spalart-Allmaras negative [^SpalartAllmarasneg] turbulence model using an Hybrid Discontinuous Galerkin (HDG) discretization.
Project website: https://antongeorg.schmid.pages.gwdg.de/spalart-allmaras-hdg/

##Software Requirement

This project is based on the finite element toolbox ngsolve [^ngsolve]. Additionally numpy is used

##Installation and Usage

We recomend using a virtual environment and installing the ngsolve and numpy from requirements.txt. For unix systems:
```
virtualenv ngsolve
source ngsolve/bin/activate
pip install -r requirements.txt
```
The driver files in "code/simulations" and various test-files can then be run using
```
netgen run.py
```

##Project structure

There are separate modules for independent parts of the simulation. They are:

- RANSclosure.SpalartAllmarasHDG, implementing the Spalart-Allmaras model,
- DNS.NavierStokes.HDG, implementing a fully implicit solver for the Navier Stokes equation, and
- eikonalsolver.eikonalsolver, used for computing euclidean distance to the closest wall in complex geometries.

Some meshes are implemented as functions and can be imported from

- geometry.geometry

Exemplary driver files are available for a Periodic Channel flow and a flat plate with a leading edge in code/simulations

[^ngsolve]: https://ngsolve.org/
[^LEE_MOSER]: https://turbulence.oden.utexas.edu/channel2015/
[^SpalartAllmarasneg]: https://turbmodels.larc.nasa.gov/spalart.html#saneg
